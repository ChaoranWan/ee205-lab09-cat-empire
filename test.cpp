///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 2.0
///
/// Unit tests
///
/// @author Andrew Wan <chaoran@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   04_05_2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>
#include "cat.hpp"

using namespace std;

int main(){
   CatEmpire cat;

   cout << "Empty: " << boolalpha << cat.empty() << endl;
   
   cout << "Add a cat" << endl;
   cat.addCat(new Cat("chad thunder"));
   cout << "Empty: " << boolalpha << cat.empty() << endl;
   
   cout << "Suffix to the 5th gen:" << endl;
   for(int i = 1; i < 6; i++){
      cat.getEnglishSuffix(i);
      cout << endl;
   }

   return 0;

}

